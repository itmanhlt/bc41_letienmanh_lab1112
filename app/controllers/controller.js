// function get information form form
function getInfoFromForm() {
  var id = document.getElementById("id").value;
  var taiKhoan = document.getElementById("TaiKhoan").value;
  var hoTen = document.getElementById("HoTen").value;
  var matKhau = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var hinhAnh = document.getElementById("HinhAnh").value;
  var loaiND = document.getElementById("loaiNguoiDung").value;
  var ngonNgu = document.getElementById("loaiNgonNgu").value;
  var moTa = document.getElementById("MoTa").value;

  return {
    id: id,
    taiKhoan: taiKhoan,
    hoTen: hoTen,
    matKhau: matKhau,
    email: email,
    hinhAnh: hinhAnh,
    loaiND: loaiND,
    ngonNgu: ngonNgu,
    moTa: moTa,
  };
}

//function render users
function renderUsers(usersArr) {
  var contentHTML = "";
  usersArr.forEach(function (user) {
    var contentTr = `<tr>
    <td>${user.id}</td>
    <td>${user.taiKhoan}</td>
    <td>${user.matKhau}</td>
    <td>${user.hoTen}</td>
    <td>${user.email}</td>
    <td>${user.ngonNgu}</td>
    <td>${user.loaiND}</td>
        <td>${user.moTa}</td>
        <td><button onclick = "deleteUsers(${user.id})" class = "btn btn-danger">Xóa</button>
        <button onclick="editUsers(${user.id})" data-toggle="modal" data-target="#myModal" class = "btn btn-warning">Sửa</button></td>
        </tr>`;
    contentHTML += contentTr;
  });
  return (document.getElementById("tblDanhSachNguoiDung").innerHTML =
    contentHTML);
}

//function get element
function getElement(element) {
  return document.getElementById(element);
}

//function show info to form
function showInfoToForm(user) {
  getElement("labelID").style.display = "block";
  getElement("id").type = "text";
  getElement("id").value = user.id;
  getElement("TaiKhoan").value = user.taiKhoan;
  getElement("HoTen").value = user.hoTen;
  getElement("MatKhau").value = user.matKhau;
  getElement("Email").value = user.email;
  getElement("HinhAnh").value = user.hinhAnh;
  getElement("loaiNguoiDung").value = user.loaiND;
  getElement("loaiNgonNgu").value = user.ngonNgu;
  getElement("MoTa").value = user.moTa;
}
