//function get element
function getElement(element) {
  return document.getElementById(element);
}

// function check empty
function checkEmpty(id, idNotify) {
  let value = getElement(id).value;
  if (value == "" || value == "0") {
    getElement(idNotify).innerHTML = "không được để trống";
    getElement(idNotify).style.display = "block";
    return false;
  } else {
    getElement(idNotify).style.display = "none";
    return true;
  }
}

// search location
function searchLocation(taiKhoan, arr) {
  var viTri = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].taiKhoan == taiKhoan) {
      viTri = i;
    }
  }
  return viTri;
}

//check duplicate
function checkDuplicate(el, idNotify, arr) {
  el = document.getElementById(el).value;
  var viTri = searchLocation(el, arr);
  if (viTri != -1) {
    getElement(idNotify).innerHTML = `Tài khoản đã tồn tại`;
    getElement(idNotify).style.display = "block";
    return false;
  } else {
    getElement(idNotify).innerHTML = "";
    getElement(idNotify).style.display = "none";
    return true;
  }
}

//check length
function checkLength(el, min, max, idNotify) {
  el = document.getElementById(el).value;
  if (el.length > max) {
    return (
      (getElement(idNotify).innerHTML = `Mô tả không vượt quá ${max} ký tự`),
      (getElement(idNotify).style.display = "block"),
      false
    );
  } else {
    return (
      (getElement(idNotify).innerHTML = ""),
      (getElement(idNotify).style.display = "none"),
      true
    );
  }
}

// check all letters
function checkLetter(el, idNotify) {
  el = document.getElementById(el).value;
  const re =
    /^[A-Za-zÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+$/;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getElement(idNotify).style.display = "none"),
      (getElement(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getElement(
        idNotify
      ).innerHTML = `Họ tên không chứa số và ký tự đặc biệt`),
      (getElement(idNotify).style.display = "block"),
      false
    );
  }
}

// check password
function checkPassword(el, idNotify) {
  el = document.getElementById(el).value;
  const re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,8}$/;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getElement(idNotify).style.display = "none"),
      (getElement(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getElement(
        idNotify
      ).innerHTML = `Mật khẩu phải có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, 1 ký tự
      số, độ dài 6-8`),
      (getElement(idNotify).style.display = "block"),
      false
    );
  }
}

// check email
function checkEmail(el, idNotify) {
  el = document.getElementById(el).value;
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isValid = re.test(el);
  if (isValid) {
    return (
      (getElement(idNotify).style.display = "none"),
      (getElement(idNotify).innerHTML = ""),
      true
    );
  } else {
    return (
      (getElement(idNotify).innerHTML = `Email phải đúng định đạng`),
      (getElement(idNotify).style.display = "block"),
      false
    );
  }
}
