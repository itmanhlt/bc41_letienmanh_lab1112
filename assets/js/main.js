const BASE_URL = "https://63bea7faf5cfc0949b5d48bf.mockapi.io";
var DSTK = [];

axios({
  url: `${BASE_URL}/users`,
  method: "GET",
})
  .then(function (res) {
    // console.log(res.data);
  })
  .catch(function (err) {
    console.log(err);
  });

// function fetch users list
function fecthUsers() {
  axios({
    url: `${BASE_URL}/users`,
    method: "GET",
  })
    .then(function (res) {
      DSTK.push(...res.data);
      renderUsers(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

fecthUsers();

//function validation
function validation() {
  let isValid = true;
  isValid =
    checkEmpty("TaiKhoan", "tbTaiKhoan") &&
    checkDuplicate("TaiKhoan", "tbTaiKhoan", DSTK);
  isValid &= checkEmpty("HoTen", "tbHoTen") && checkLetter("HoTen", "tbHoTen");
  isValid &=
    checkEmpty("MatKhau", "tbMatKhau") && checkPassword("MatKhau", "tbMatKhau");
  isValid &= checkEmpty("Email", "tbEmail") && checkEmail("Email", "tbEmail");
  isValid &= checkEmpty("HinhAnh", "tbHinhAnh");
  isValid &= checkEmpty("loaiNguoiDung", "tbLoaiNguoiDung");
  isValid &= checkEmpty("loaiNgonNgu", "tbNgonNgu");
  isValid &=
    checkEmpty("MoTa", "tbMoTa") && checkLength("MoTa", 0, 60, "tbMoTa");
  return isValid;
}

// function add users
function addUsers() {
  let isValid = validation();
  if (isValid) {
    axios({
      url: `${BASE_URL}/users`,
      method: "POST",
      data: getInfoFromForm(),
    })
      .then(function (res) {
        fecthUsers();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

// function remove users
function deleteUsers(id) {
  axios({
    url: `${BASE_URL}/users/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fecthUsers();
    })
    .catch(function (err) {
      console.log(err);
    });
}

// function edit users
function editUsers(id) {
  document.getElementById("capNhat").disabled = false;
  document.getElementById("btnThem").disabled = true;
  axios({
    url: `${BASE_URL}/users/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showInfoToForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

//function updateUsers
function updateUsers() {
  let data = getInfoFromForm();
  let isValid = true;
  isValid = checkEmpty("TaiKhoan", "tbTaiKhoan");
  isValid &= checkEmpty("HoTen", "tbHoTen") && checkLetter("HoTen", "tbHoTen");
  isValid &=
    checkEmpty("MatKhau", "tbMatKhau") && checkPassword("MatKhau", "tbMatKhau");
  isValid &= checkEmpty("Email", "tbEmail") && checkEmail("Email", "tbEmail");
  isValid &= checkEmpty("HinhAnh", "tbHinhAnh");
  isValid &= checkEmpty("loaiNguoiDung", "tbLoaiNguoiDung");
  isValid &= checkEmpty("loaiNgonNgu", "tbNgonNgu");
  isValid &=
    checkEmpty("MoTa", "tbMoTa") && checkLength("MoTa", 0, 60, "tbMoTa");
  if (isValid) {
    axios({
      url: `${BASE_URL}/users/${data.id}`,
      method: "PUT",
      data: data,
    })
      .then(function (res) {
        fecthUsers(res.data);
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}
